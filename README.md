*************
Assignent completed by: 
      David Miller	50039xxxx
      Ben Verna-Asals	50049xxxx

*************

###COMPILING & RUNNING###

This program is to be compiled and run on a linux machine.
There is a makefile included in this directory. Run it like any other makefile.
ie:
```
#!bash
make
```
And to remove all object files, exectuables, etc, run:
```
#!bash        
make clean
```
This will produce an exectuable named 'Game'. Run with:
```
#!bash  	
./Game
```
All required features were implemented.
BONUS: Imported 'cool' looking robot designed using a modelling package

###GAME CONTROLS###
```
Arrow keys:	move around
Space Bar:	Shoot
r:    		Reset game
```

###SECRET CONTROLS (used for debugging and test, not for gameplay)###
```
b:     		generate a new bot in one room
F1:		go into arial view over player
Left Mouse btn:	Hold and drag to view from above
```